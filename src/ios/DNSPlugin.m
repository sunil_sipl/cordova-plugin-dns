#import "DNSPlugin.h"
#include <arpa/inet.h>
#include <netdb.h>

#if !defined(_POSIX_C_SOURCE) || defined(_DARWIN_C_SOURCE)
#define    NI_MAXHOST    1025
#define    NI_MAXSERV    32
#endif /* (!_POSIX_C_SOURCE || _DARWIN_C_SOURCE) */

static NSString * _Nonnull numericStringForAddress(NSData * _Nonnull address) {
    char name[NI_MAXHOST];
    
    BOOL success = getnameinfo(address.bytes, (socklen_t) address.length, name, sizeof(name), NULL, 0, NI_NUMERICHOST | NI_NUMERICSERV) == 0;
    if ( ! success ) {
        return @"?";
    }
    return @(name);
}

@implementation DNSPlugin

- (void)resolve:(CDVInvokedUrlCommand*)command {

    CDVPluginResult *result;
    NSString *hostname = [command.arguments objectAtIndex:0];
    CFHostRef hostref = CFHostCreateWithName(kCFAllocatorDefault, (__bridge CFStringRef)hostname);
    if (hostref) {
        Boolean lookupResult = CFHostStartInfoResolution(hostref, kCFHostAddresses, NULL);
        if (lookupResult == TRUE) {
           
             NSArray<NSData *> * addresses = (__bridge NSArray<NSData *> *) CFHostGetAddressing(hostref, &lookupResult);
            NSMutableArray<NSString *> * stringAddresses = [[NSMutableArray alloc] init];
            for (NSData * address in addresses) {
                [stringAddresses addObject: numericStringForAddress(address) ];
            }
            

            if (lookupResult == TRUE && [stringAddresses count] > 0) {
                NSString * addrstr = [stringAddresses objectAtIndex:0];
                result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:addrstr];
            }
            
        }
    }

    if (result == NULL) {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Not resolved"];
    }

    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

@end
